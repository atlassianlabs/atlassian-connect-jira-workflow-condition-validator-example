# Workflow Conditions and Validators Example

## Features

This project serves as an end-to-end example of how to implement custom workflow validators and conditions in Jira Cloud.
It's a Connect app that provides one workflow condition and one workflow validator:

* The workflow condition shows a transition only if the issue summary matches a regular expression provided by the user.
* The workflow validator allows a transition only if the issue contains a comment with body that matches a regular expression provided by the user.

## Getting started

This project uses the [atlassian-connect-spring-boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot) framework. 
To build it, call `mvn install` ([Maven Version Manager](https://bitbucket.org/atlassian/atlassian-connect-spring-boot) is recommended), then run the following command:

    mvn spring-boot:run
    
The application should start up locally on port 8080. If you visit `http://localhost:8080/atlassian-connect.json` in your
browser, you should see the app descriptor.

You can enable debugging and set custom port or base URL by providing additional arguments to the command, for example:

     mvn spring-boot:run \
     -Drun.jvmArguments="-Dserver.port=8081 -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5006" \
     -Drun.arguments="--addon.base-url=https://352a5473.eu.ngrok.io"

## Getting help

If you need help developing against Atlassian products, see the [Atlassian Developer](https://developer.atlassian.com/) site.


For help developing workflow validators and conditions, see documentation for the relevant Connect modules:

* [workflow condition](https://developer.atlassian.com/cloud/jira/platform/modules/workflow-condition/)
* [workflow validator](https://developer.atlassian.com/cloud/jira/platform/modules/workflow-validator/)

If you want to report a problem, please raise a support request in Atlassian Ecosystem's
[Developer Service Desk](http://go.atlassian.com/ecosystem-developer-support).

## Contributing

Pull requests are always welcome.


## License

This project is licensed under the [Apache License, Version 2.0](LICENSE.txt).
